import QtQuick 2.2

import "components"

FocusScope {
    property alias source: image.source
    property alias fillMode: image.fillMode
    property alias status: image.status
    
    Rectangle {
            width: parent.width / 60
            height: parent.height / 32
            color: "transparent"
        
        Image {
            id:dropImage
            width: parent.width
            height: parent.height
            source: "images/down.png"
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: { parent.color = "white" }
            onExited: { parent.color = "transparent" }
            onClicked: { lid.state = "down" }
        }
    }
        
    Rectangle {
        id: lid
        width: parent.width
        height: parent.height
        color: "transparent"
        
        Image {
            id: image
            width: parent.width
            height: parent.height

            clip: true
            focus: true
            smooth: true
            
            MouseArea {
                anchors.fill: parent
                onClicked: { lid.state = "up" }
            }

            Clock0 {
                visible: y > 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: units.gridUnit * 5
                anchors.left: parent.left
                anchors.leftMargin: units.gridUnit * 5
            }
        }

        // http://doc.qt.io/qt-5/qml-qtquick-state.html
        // http://doc.qt.io/qt-5/qml-qtquick-numberanimation.html
        states: [
            State {
                name: "up"
                PropertyChanges { target: image; y: - parent.height }
                PropertyChanges { target: dropImage; visible: true }
            },
            State {
                name: "down"
                PropertyChanges { target: image; y: 0 }
            }
        ]
    
        transitions: [
            Transition {
                from: "*"; to: "up"
                NumberAnimation { properties: "y" }
            },
            Transition {
                from: "*"; to: "down"
                NumberAnimation { properties: "y" }
            }
        ]
    }
}
