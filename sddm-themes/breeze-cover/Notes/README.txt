This sddm theme is based on the KDE Breeze theme - plasma-workspace-5.8.4


Installation:

Plasma 4.9 or later

Use "Get New Theme / Install From file" from the KDE SDDM control module (kcm-sddm).


Older than Plasma 4.9

Unpack and copy the 'breeze-cover' directory to the /usr/share/sddm/themes/. From the cli:

$ sudo cp -R breeze-cover /usr/share/sddm/themes/

A YouTube clip: https://youtu.be/Ew4HR3GvnB8


Testing: 

https://github.com/sddm/sddm/wiki/Theming
'You can test your themes using sddm-greeter. Note that in this mode, actions like shutdown, suspend or login will have no effect.

sddm-greeter --theme /path/to/you/theme'


Errors:

The testing should show if you have all the bit and pieces.


Cover image

The cover image can be set from the KDE SDDM control module ($ kcmshell5 sddm). Mouse left click will
show the log in screen: https://youtu.be/Ew4HR3GvnB8 .


Login screen background

The login screen background can be set by copying the wanted jpg image to the '../breeze-cover/images/LoginImage.jpg'.
From the cli:

$ sudo cp image.jpg /usr/share/sddm/themes/breeze-cover/images/LoginImage.jpg


Changes:

Removing:
Background.qml
Clock.qml

Moving:
preview.png -> /images/preview.png

Edit:
metadata.desktop
Main.qml
theme.conf

Add:
Cover.qml
/components/Clock0.qml
/images/Cover.jpg
/images/down.png


KDE Forum: https://forum.kde.org/viewtopic.php?f=289&t=137502
